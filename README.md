### To run this service follow below steps-

copy service file in service storage directory.
### *#cp ./noshutscript.service /etc/systemd/system*

Copy ntest.sh file in which location you want then edit the service file accordingly-
for now i'm copying in my home directory-
### *#cp ./ntest.sh /home/ubuntu*

Edit service file and change the location in "ExecStart" line-

### *#vim /etc/systemd/system/noshutscript.service*
>> ExecStart=/bin/sh /home/ubuntu/ntest.sh

Now reload the daemon and enable the service-
### *#systemctl daemon-reload*

### ----To enable the service----

### *systemctl enable noshutscript.service*

Note:- reboot and enjoy
# reboot
